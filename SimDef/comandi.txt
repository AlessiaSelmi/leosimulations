# =========================================================================
# Da ovunque, per forzare il numero di thread in parallelo di G4
export G4FORCENUMBEROFTHREADS=4

# =========================================================================
# Source, dalla cartella CERN2022-build
## Local: ROOT & G4
source ~/Geant4.11.0.2/geant4-11.0.2-install/bin/geant4.sh
source ~/root/bin/thisroot.sh

## On VM
source /cvmfs/sft.cern.ch/lcg/contrib/gcc/9/x86_64-centos8/setup.sh
source /cvmfs/geant4.cern.ch/geant4/11.0.p02/x86_64-centos8-gcc9-optdeb-MT/GNUMake-setup.sh
source /cvmfs/sft.cern.ch/lcg/contrib/CMake/3.18.3/Linux-x86_64/setup.sh
source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.28.00/x86_64-centos8-gcc85-opt/bin/thisroot.sh

# =========================================================================
# Make, dalla cartella CERN2022-build
## Local
cmake -DGeant4_DIR=~/Geant4.11.0.2/geant4-11.0.2-install/lib/Geant4-11.0.2/ ../CERN2022 && make -j4

## On VM
cmake -DGeant4_DIR=/cvmfs/geant4.cern.ch/geant4/11.0.p02/x86_64-centos8-gcc9-optdeb-MT/lib64/Geant4-11.0.2/ ../CERN2022 && make -j4

# =========================================================================
# Call, dalla cartella CERN2022-build
## Singola run
./klever21 macros/run_forphysics.mac

## Run grafica
./klever21
/control/execute macros/run_forphysics.mac

# =========================================================================
# Pulizia della cartella di build
python3 ../build_cleaner.py

# =========================================================================
# Hadd degli output
hadd tbeamoutput.root tbeamdata000*

# =========================================================================
# Mastercall
python3 ../build_cleaner.py && cmake -DGeant4_DIR=~/Geant4.11.0.2/geant4-11.0.2-install/lib/Geant4-11.0.2/ ../CERN2022 && make -j4 && ./klever21 macros/run_forphysics.mac
