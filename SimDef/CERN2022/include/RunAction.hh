#ifndef RUNACTION_HH
#define RUNACTION_HH

#include <G4SystemOfUnits.hh>
#include <G4UserRunAction.hh>
#include <G4Run.hh>

#include "G4Accumulable.hh"
#include "G4AutoLock.hh"
#include <vector>
#include "globals.hh"
#include <fstream>

// RunAction, actions executed at each run

class RunAction : public G4UserRunAction
{
public:
    RunAction();
    ~RunAction();
    void BeginOfRunAction(const G4Run*) override;
    void EndOfRunAction(const G4Run*);
  
private:
    // event-by-event scoring for test simulation (implemented in include/TestMode.cc)
    //void OutputNtupleTest(G4AnalysisManager* analysis);
};

#endif
