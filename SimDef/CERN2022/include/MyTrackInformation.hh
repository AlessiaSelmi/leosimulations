#ifndef MY_TRACK_INFORMATION_HH
#define MY_TRACK_INFORMATION_HH
#include "G4VUserTrackInformation.hh"


// Set a variable to true if the track have been tracked in z
class MyTrackInformation : public G4VUserTrackInformation {
public:
    MyTrackInformation() : G4VUserTrackInformation(), haveBeenTracked(false) {}

    void SetHaveBeenTracked(G4bool value) {
        haveBeenTracked = value;
    }

    G4bool GetHaveBeenTracked() const {
        return haveBeenTracked;
    }

private:
    G4bool haveBeenTracked;
};

#endif