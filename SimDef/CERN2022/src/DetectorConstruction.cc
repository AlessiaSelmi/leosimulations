// Standard includes
#include <G4SystemOfUnits.hh>
#include <G4LogicalVolume.hh>
#include <G4PVPlacement.hh>
#include <G4NistManager.hh>
#include <G4SystemOfUnits.hh>
#include <G4VisAttributes.hh>
#include <G4SDManager.hh>
#include <G4LogicalVolumeStore.hh>
#include <G4UniformMagField.hh>
#include <G4FieldManager.hh>
#include <G4TransportationManager.hh>
#include <G4ChordFinder.hh>
#include <G4MultiFunctionalDetector.hh>
#include <G4Box.hh>

#include "DetectorConstruction.hh"
#include "CustomSD.hh"

#include <G4Tubs.hh>
#include <G4SubtractionSolid.hh>

#include "LogicalCrystalVolume.hh"  // added to exploit modified Geant4

#include "G4ProductionCuts.hh"
#include "G4RegionStore.hh"

DetectorConstruction::DetectorConstruction()
:G4VUserDetectorConstruction(),
 fpRegion(0)
{}

// DetectorConstruction::Construct, i.e. where the setup geometry is implemented
G4VPhysicalVolume* DetectorConstruction::Construct()
{
    // ============================================================================
    //                                 MATERIALS DEFINITION
    // ============================================================================
    // Retrieve the NIST database for materials
    G4NistManager* nist = G4NistManager::Instance();
    
    // Define if the crystal under test must be placed or not
    // (Note to self (2023.03.03): if not -> useful for calibration runs, 
    // I suppose? Unclear in the original code...)
    //G4bool bCrys = true; // crystal placement switch
	
    // Create the colors used in the visualization
    G4VisAttributes* grey = new G4VisAttributes(true, G4Colour::Grey());
    G4VisAttributes* cyan = new G4VisAttributes(true, G4Colour::Cyan());
	G4VisAttributes* green = new G4VisAttributes(true, G4Colour::Green());
	G4VisAttributes* red = new G4VisAttributes(true, G4Colour::Red());
	//G4VisAttributes* white = new G4VisAttributes(true, G4Colour::White());
        G4VisAttributes* black = new G4VisAttributes(true, G4Colour::Black());
	//G4VisAttributes* invisible = new G4VisAttributes(false);
	//G4VisAttributes* brown = new G4VisAttributes(true, G4Colour::Brown());
    G4VisAttributes* blue = new G4VisAttributes(true, G4Colour::Blue());

    // Create the most important materials
    G4Material* air = nist->FindOrBuildMaterial("G4_AIR");      // air
    G4Material* silicon = nist->FindOrBuildMaterial("G4_Si");   // silicon
    //G4Material* aluminum = nist->FindOrBuildMaterial("G4_Al");  // aluminum
    //G4Material* iron = nist->FindOrBuildMaterial("G4_Fe");      // iron 
    G4Material* pwo = nist->FindOrBuildMaterial("G4_PbWO4");    // lead tungstate
    //G4Material* bgo = nist->FindOrBuildMaterial("G4_BGO");      // BGO
    G4Material* PbO = nist->FindOrBuildMaterial("G4_GLASS_LEAD");// lead glass
    //G4Material* plastic = nist->FindOrBuildMaterial("G4_POLYSTYRENE"); // polystirene, for scintillators 
    //G4Material* copper= nist->FindOrBuildMaterial("G4_Cu");     // copper

    // world
    G4double worldSizeX = 10 * m;
    G4double worldSizeY = 10 * m;
    G4double worldSizeZ = 80 * m;
    G4VSolid* worldBox = new G4Box("World_Solid", worldSizeX / 2, worldSizeY / 2, worldSizeZ / 2);

    G4LogicalVolume* worldLog = new G4LogicalVolume(worldBox, air, "World_Logical");
    G4VisAttributes* visAttrWorld = new G4VisAttributes();
    visAttrWorld->SetVisibility(false);
    worldLog->SetVisAttributes(visAttrWorld);

    G4VPhysicalVolume* worldPhys = new G4PVPlacement(nullptr, {}, worldLog, "World", nullptr, false, 0);
	
    // ============================================================================
    //                                 DISTANCES
    // ============================================================================
    // Define the detectors positions along the beam axis (z)
    // (ALL THE DETECTORS)
    G4double zTrackerSiliCentre0 = 0 * cm;     // Tele1 (module 0) longitudinal centre (4.5 cm long)
    G4double zTrackerSiliCentre1 = 1548 * cm;  // Tele2 (module 1) longitudinal centre (4.5 cm long)
    G4double zTgt = (1548 + 60) * cm;          // Crystal under test (target) longitudinal centre
    G4double zPhCaloFront = (1548 + 60 + 3) * cm;  // Lead glass longitudinal front
    //G4double zPhCaloFront = 50*cm;
    
    // ============================================================================
    //                                 CRYSTAL TARGET
    // ============================================================================
    // Define the dimensions of the crystal target (consider the samples used in OREO 08/2023)
    G4double tgtThickness = 5 * pwo->GetRadlen();  // z
    G4double tgtWidth = 2.5 * cm;              // x
    G4double tgtHeight = 2.5 * cm;             // y

    // Construct the physical volume    
    G4VSolid* tgtBox = new G4Box("Tgt_Solid", tgtWidth / 2, tgtHeight / 2, tgtThickness / 2);
	
    // Construct the logical volume, considering if we are running in "axial" or "amorphous" mode
    G4LogicalVolume* tgtLogA = new G4LogicalVolume(tgtBox, pwo, "Tgt_LogicalA");
    G4LogicalVolume* tgtLogB = new G4LogicalVolume(tgtBox, pwo, "Tgt_LogicalB");
    G4LogicalVolume* tgtLogC = new G4LogicalVolume(tgtBox, pwo, "Tgt_LogicalC");

    tgtLogB->SetVisAttributes(green);
    tgtLogA->SetVisAttributes(red);
    tgtLogC->SetVisAttributes(green);
    
    // Place the crystal physical volume
    //leo sposto cristallo
    //new G4PVPlacement(nullptr, G4ThreeVector(-tgtWidth+1000 , 0+1000 , zTgt + 20 * m ), tgtLogA, "Tgt_SolidA", worldLog, false, 0);
    //new G4PVPlacement(nullptr, G4ThreeVector(0+1000, 0+1000 , zTgt + 20 * m ), tgtLogB, "Tgt_SolidB", worldLog, false, 0);

    new G4PVPlacement(nullptr, G4ThreeVector(0, 0  , zTgt + tgtThickness / 2  ), tgtLogA, "Tgt_SolidA", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(-tgtWidth , 0  , zTgt + tgtThickness / 2  ), tgtLogB, "Tgt_SolidB", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(+tgtWidth,0 , zTgt + tgtThickness / 2), tgtLogC, "Tgt_SolidC", worldLog, false, 0); 
    


    if(bChanneling){
        // "Axial" mode. Add the crystal target region (used for localized application of SF interactions)
        G4Region* fpRegion2A = new G4Region("SF_Region_A");
        fpRegion2A->AddRootLogicalVolume(tgtLogA);
        G4Region* fpRegion2B = new G4Region("SF_Region_B");
        fpRegion2B->AddRootLogicalVolume(tgtLogB);
        G4Region* fpRegion2C = new G4Region("SF_Region_C");
        fpRegion2C->AddRootLogicalVolume(tgtLogC);
    }

    // ============================================================================
    //                                 OTHER VOLUMES
    // ============================================================================

    // Place the silicon trackers
    G4double trackerSiliThickness = 410 * um; ;     // Chambers
    G4double trackerSiliWidth = 384 * 242 * um;
    G4double trackerSiliHeight = 384 * 242 * um;
    G4VSolid* trackerSiliBox = new G4Box("TrackerSili_Solid",  trackerSiliWidth / 2, trackerSiliHeight / 2, trackerSiliThickness / 2);
    
    G4LogicalVolume* trackerSiliLog0 = new G4LogicalVolume(trackerSiliBox, silicon, "TrackerSili_Logical_0");  // tracker 0 is small
    G4LogicalVolume* trackerSiliLog1 = new G4LogicalVolume(trackerSiliBox, silicon, "TrackerSili_Logical_1");  // tracker 1 is small
    trackerSiliLog0->SetVisAttributes(grey);
    trackerSiliLog1->SetVisAttributes(grey);
    
	
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTrackerSiliCentre0), trackerSiliLog0, "TrackerSili_0", worldLog, false, 0);
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0, zTrackerSiliCentre1), trackerSiliLog1, "TrackerSili_1", worldLog, false, 0);
    
	
    // Lead Glass omogeneous  calorimeter
    G4double caloPhThickness = 24.7 * PbO->GetRadlen(); //
    G4double caloPhWidthChannel = 10 * cm;
    G4double caloPhHeightChannel = 10 * cm;
    G4VSolid* caloPhChannelBox = new G4Box("PhCalTest_Solid", caloPhWidthChannel / 2, caloPhHeightChannel / 2, caloPhThickness / 2);
	
    G4LogicalVolume* caloPhLog_CC = new G4LogicalVolume(caloPhChannelBox, PbO, "PhCalTest_CC_Logical");

    // leo aggiungo un calorimetro TopLeft volume logico
    G4LogicalVolume* caloPhLog_TL = new G4LogicalVolume(caloPhChannelBox, PbO, "PhCalTest_TL_Logical");

    // leo aggiungo un calorimetro TopRight volume logico
    G4LogicalVolume* caloPhLog_TR = new G4LogicalVolume(caloPhChannelBox, PbO, "PhCalTest_TR_Logical");

    // leo aggiungo un calorimetro BottomLeft volume logico
    G4LogicalVolume* caloPhLog_BL = new G4LogicalVolume(caloPhChannelBox, PbO, "PhCalTest_BL_Logical");

    // leo aggiungo un calorimetro CenterLeft volume logico
    G4LogicalVolume* caloPhLog_CL = new G4LogicalVolume(caloPhChannelBox, PbO, "PhCalTest_CL_Logical");

    // leo aggiungo un calorimetro CenterRight volume logico
    G4LogicalVolume* caloPhLog_CR = new G4LogicalVolume(caloPhChannelBox, PbO, "PhCalTest_CR_Logical");

    // leo aggiungo un calorimetro BottomRight volume logico
    G4LogicalVolume* caloPhLog_BR = new G4LogicalVolume(caloPhChannelBox, PbO, "PhCalTest_BR_Logical");
    
    
    //leo aggiungo calo colore
    caloPhLog_CC->SetVisAttributes(black);
    caloPhLog_TL->SetVisAttributes(black);
    caloPhLog_TR->SetVisAttributes(black);
    caloPhLog_BL->SetVisAttributes(black);
    caloPhLog_CL->SetVisAttributes(black);
    caloPhLog_BR->SetVisAttributes(black);
    caloPhLog_CR->SetVisAttributes(black);
	
    new G4PVPlacement(nullptr, G4ThreeVector(0, 0,  zPhCaloFront + caloPhThickness / 2), caloPhLog_CC, "PhCalTest_CC", worldLog, false, 0);

    // leo aggiungo calo TopLeft posizionamento 
    new G4PVPlacement(nullptr, G4ThreeVector(0 + caloPhWidthChannel/2, 0 + caloPhHeightChannel,  zPhCaloFront + caloPhThickness / 2), caloPhLog_TL, "PhCalTest_TL", worldLog, false, 0);

    // leo aggiungo calo TopRight posizionamento
    new G4PVPlacement(nullptr, G4ThreeVector(0 - caloPhWidthChannel/2, 0 + caloPhHeightChannel,  zPhCaloFront + caloPhThickness / 2), caloPhLog_TR, "PhCalTest_TR", worldLog, false, 0);

    // leo aggiungo calo BottomLeft posizionamento
    new G4PVPlacement(nullptr, G4ThreeVector(0 + caloPhWidthChannel / 2, 0 - caloPhHeightChannel,  zPhCaloFront + caloPhThickness / 2), caloPhLog_BL, "PhCalTest_BL", worldLog, false, 0);

    // leo aggiungo calo CenterLeft posizionamento
    new G4PVPlacement(nullptr, G4ThreeVector(0 + caloPhWidthChannel, 0,  zPhCaloFront + caloPhThickness / 2), caloPhLog_CL, "PhCalTest_CL", worldLog, false, 0);

    // leo aggiungo calo CenterRight posizionamento
    new G4PVPlacement(nullptr, G4ThreeVector(0 - caloPhWidthChannel, 0,  zPhCaloFront + caloPhThickness / 2), caloPhLog_CR, "PhCalTest_CR", worldLog, false, 0);


    // leo aggiungo calo BottomRight posizionamento
    new G4PVPlacement(nullptr, G4ThreeVector(0 - caloPhWidthChannel / 2, 0 - caloPhHeightChannel,  zPhCaloFront + caloPhThickness / 2), caloPhLog_BR, "PhCalTest_BR", worldLog, false, 0);

    	


	// ============================================================================
    //                                 CONCLUDE
    // ============================================================================
    // Print list of defined material
    G4cout << "-----" << G4endl;
    G4cout << "| DetectorConstruction.cc: material list" << G4endl;
    G4cout << *(G4Material::GetMaterialTable()) << G4endl;
    G4cout << "-----" << G4endl;
	
    return worldPhys;
}

// DetectorConstruction::ConstructSDandField, i.e. where the sensitive detectors and magnetic fields are implemented
void DetectorConstruction::ConstructSDandField()
{
    // ============================================================================
    //                                 Sensitive Detectors
    // ============================================================================
    // Load the sensitive detector manager
    G4SDManager* sdm = G4SDManager::GetSDMpointer();
    //sdm->SetVerboseLevel(1);  // set sensitive detector manager verbosity here
    sdm->SetVerboseLevel(0);  // set sensitive detector manager verbosity here
    
    // SD: gamma calorimeter
    VolumeEDepSD* gammaCalSD_CC = new VolumeEDepSD("PhCalTest_CC_SD");
    SetSensitiveDetector("PhCalTest_CC_Logical", gammaCalSD_CC);
    sdm->AddNewDetector(gammaCalSD_CC);

    // leo aggiungo calo sensitive detector TopLeft
    // SD: gamma calorimeter
    VolumeEDepSD* gammaCalSD_TL = new VolumeEDepSD("PhCalTest_TL_SD");
    SetSensitiveDetector("PhCalTest_TL_Logical", gammaCalSD_TL);
    sdm->AddNewDetector(gammaCalSD_TL);

    // leo aggiungo calo sensitive detector TopRight
    // SD: gamma calorimeter
    VolumeEDepSD* gammaCalSD_TR = new VolumeEDepSD("PhCalTest_TR_SD");
    SetSensitiveDetector("PhCalTest_TR_Logical", gammaCalSD_TR);
    sdm->AddNewDetector(gammaCalSD_TR);

    // leo aggiungo calo sensitive detector BottomLeft
    // SD: gamma calorimeter
    VolumeEDepSD* gammaCalSD_BL = new VolumeEDepSD("PhCalTest_BL_SD");
    SetSensitiveDetector("PhCalTest_BL_Logical", gammaCalSD_BL);
    sdm->AddNewDetector(gammaCalSD_BL);

    // leo aggiungo calo sensitive detector CenterLeft
    // SD: gamma calorimeter
    VolumeEDepSD* gammaCalSD_CL = new VolumeEDepSD("PhCalTest_CL_SD");
    SetSensitiveDetector("PhCalTest_CL_Logical", gammaCalSD_CL);
    sdm->AddNewDetector(gammaCalSD_CL);

    // leo aggiungo calo sensitive detector CenterRight
    // SD: gamma calorimeter
    VolumeEDepSD* gammaCalSD_CR = new VolumeEDepSD("PhCalTest_CR_SD");
    SetSensitiveDetector("PhCalTest_CR_Logical", gammaCalSD_CR);
    sdm->AddNewDetector(gammaCalSD_CR);

    // leo aggiungo calo sensitive detector BottomRight
    // SD: gamma calorimeter
    VolumeEDepSD* gammaCalSD_BR = new VolumeEDepSD("PhCalTest_BR_SD");
    SetSensitiveDetector("PhCalTest_BR_Logical", gammaCalSD_BR);
    sdm->AddNewDetector(gammaCalSD_BR);

    
    // SD: crystal under test
    VolumeEDepSD* tgtSD_22A = new VolumeEDepSD("TgtA_SD");
    SetSensitiveDetector("Tgt_LogicalA", tgtSD_22A);
    sdm->AddNewDetector(tgtSD_22A);
    
    VolumeEDepSD* tgtSD_22B = new VolumeEDepSD("TgtB_SD");
    SetSensitiveDetector("Tgt_LogicalB", tgtSD_22B);
    sdm->AddNewDetector(tgtSD_22B);
    
    VolumeEDepSD* tgtSD_22C = new VolumeEDepSD("TgtC_SD");
    SetSensitiveDetector("Tgt_LogicalC", tgtSD_22C);
    sdm->AddNewDetector(tgtSD_22C);

	
    // SD: silicon trackers (1 sensitive detector per tracking plane)
    // (meaning 1 SD for each telescope and 2 SD for each chamber)
    VolumeTrackingSD* trackerSD0 = new VolumeTrackingSD("Tracker_SD_0");
    SetSensitiveDetector("TrackerSili_Logical_0", trackerSD0);
    sdm->AddNewDetector(trackerSD0);
    VolumeTrackingSD* trackerSD1 = new VolumeTrackingSD("Tracker_SD_1");
    SetSensitiveDetector("TrackerSili_Logical_1", trackerSD1);
    sdm->AddNewDetector(trackerSD1);

}
