#include "ActionInitialization.hh"
#include "PrimaryGeneratorAction.hh"
#include "RunAction.hh"
#include "EventAction.hh"
#include "SteppingAction.hh"
#include "MyMessenger.hh"

// ActionInitialization, i.e. where the action classes are conjured

void ActionInitialization::Build() const
{
  SetUserAction(new PrimaryGeneratorAction);

  RunAction* runAction = new RunAction;
  SetUserAction(runAction);

  EventAction* eventAction = new EventAction();
  SetUserAction(eventAction);

  SteppingAction* steppingAction = new SteppingAction(eventAction);
  SetUserAction(steppingAction);

  new MyMessenger(steppingAction);

}



// just for multithreading
void ActionInitialization::BuildForMaster() const
{SetUserAction(new RunAction());}
