#include "SteppingAction.hh"
#include "MyMessenger.hh"
#include <sstream>
#include <G4SystemOfUnits.hh>


MyMessenger::MyMessenger(SteppingAction* sa)
    : G4UImessenger(), mySteppingAction(sa)
{
    myDir = new G4UIdirectory("/myCommand/");
    myDir->SetGuidance("Custom commands for MySteppingAction");

    myFloatCmd = new G4UIcmdWithADoubleAndUnit("/myCommand/zProfile", this);
    myFloatCmd->SetGuidance("Set the float number for MySteppingAction");
    myFloatCmd->AvailableForStates(G4State_PreInit, G4State_Idle);


    myRadiusCmd = new G4UIcmdWithADoubleAndUnit("/myCommand/radiusProfile", this);
    myRadiusCmd->SetGuidance("Set the float number for MySteppingAction");
    myRadiusCmd->AvailableForStates(G4State_PreInit, G4State_Idle);
}

MyMessenger::~MyMessenger()
{
    delete myFloatCmd;
    delete myDir;
    delete myRadiusCmd;
}

void MyMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{
    if (command == myFloatCmd) {
        // Without unit
        // mySteppingAction->SetZFromMacro(myFloatCmd->GetNewDoubleValue(newValue));

        /*
        G4double value;
        G4String unit;
        G4String unit2;
        std::istringstream iss(newValue);
        iss >> value >> unit >> unit2;


        // Convert the value to a consistent unit (e.g., mm) based on the input unit
        if (unit == "cm") {
            G4cout << "I am converting to centimeters" << G4endl;
            G4cout << "Value was --- " << value << G4endl;
            value *= cm;
            G4cout << "Value is --- " << value << G4endl;
        } else if (unit == "m") {
            G4cout << "I am converting to meters" << G4endl;
            G4cout << "Value was --- " << value << G4endl;
            value *= m;
            G4cout << "Value is --- " << value << G4endl;
        } // Add more unit options as needed


        // Set the value in MySteppingAction
        mySteppingAction->SetZFromMacro(value);
        */



        // G4cout << "New double value" << myFloatCmd->GetNewDoubleValue(newValue) << G4endl;
        // G4cout << "New  unit value" << myFloatCmd->GetNewUnitValue(newValue) << G4endl;


        // Automatic manages unit
        // Valore convertito in unità standard
        // GetNewUnitValue contiene invece il coefficiente di riscalamento
        G4double valueDefaultUnit = myFloatCmd->GetNewDoubleValue(newValue); 

        // Set the value in MySteppingAction
        mySteppingAction->SetZFromMacro(valueDefaultUnit);

    }

    if (command == myRadiusCmd) {

        /*
        G4double value;
        G4String unit;
        std::istringstream iss(newValue);
        iss >> value >> unit;

        // Convert the value to a consistent unit (e.g., mm) based on the input unit
        if (unit == "cm") {
            value *= cm;
        } else if (unit == "m") {
            value *= m;
        } // Add more unit options as needed

        // Set the value in MySteppingAction
        mySteppingAction->SetRadiusFromMacro(value);
        */

        G4double valueDefaultUnit = myRadiusCmd->GetNewDoubleValue(newValue);

        // Set the value in MySteppingAction
        mySteppingAction->SetRadiusFromMacro(valueDefaultUnit);
    }
}


