//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file SteppingAction.cc
/// \brief Implementation of the B1::SteppingAction class
#include "SteppingAction.hh"
#include "EventAction.hh"
#include "DetectorConstruction.hh"
#include "G4Step.hh"
#include "G4Event.hh"
#include "G4RunManager.hh"
#include "G4LogicalVolume.hh"
#include "G4SystemOfUnits.hh"
#include "G4AnalysisManager.hh"

#include "G4ParticleDefinition.hh"
#include "G4ParticleTable.hh"

#include "G4SteppingManager.hh"
#include "MyTrackInformation.hh"

//leo deposito energetico per fare potenza

#include <iostream>
#include <cmath>

// leo variabili per decidere in che modalita stiamo lavorando potrei aggiungere una protezione riguardante queste variabili...

 G4bool DepEn = false; // true se stiamo lavorando per sapere energie esatte di tutte le particelle primarie e secondarie per z>zFromMacro & r>r>rFromMacro.

 G4bool TrackerHisto = true; // true se stiamo lavorando per avere tracking con istogrammi.

 G4bool Tracker = false; // true se stiamo lavorando per avere senza istogrammare, consigliabile solo con pochissimi eventi



//namespace B1
//{
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
SteppingAction::SteppingAction(EventAction* eventAction)
: fEventAction(eventAction), zFromMacro(0.0), radiusFromMacro(0.0)
{}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
SteppingAction::~SteppingAction()
{}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void SteppingAction::SetZFromMacro(G4double value) {
    zFromMacro = value;
}

G4double SteppingAction::GetZFromMacro() const {
    return zFromMacro;
}

void SteppingAction::SetRadiusFromMacro(G4double value) {
    radiusFromMacro = value;
}

G4double SteppingAction::GetRadiusFromMacro() const {
    return radiusFromMacro;
}

// Get the position at which compute profile
// G4double SteppingAction::GetZprofile() const {
//     return zProfile;
// }




// leo MODALITA tracking. qui dentro trovi sia se vuoi isto sia se vuoi salvare evento per evento

void SteppingAction::UserSteppingAction(const G4Step* step)
{

    // Oggetto per scrivere nel file
    G4AnalysisManager* analysis = G4AnalysisManager::Instance();
    analysis->SetVerboseLevel(2);  // set analysis manager verbosity here


    // Ottengo la traccia
    G4Track* track = step->GetTrack();

// leo questa parte serve solo se scelgo di tracciare! quindi se scelgo di tracciare con isto O evento per evento! 

  if((Tracker == true) or (TrackerHisto == true) ){
    MyTrackInformation* myTrackInfo;
    myTrackInfo = dynamic_cast< MyTrackInformation*>(track->GetUserInformation());
   if (! myTrackInfo) {
        // Se gliel'ho gia appicicato
        // Prendi solo il valore
        
   
        // La traccia potrebbe non avere informazioni utente

        // Altrimenti
        // Creo l'oggetto MyTrackInformation
        // Lo appicico alla traccia


        myTrackInfo = new MyTrackInformation(); // Default to false
        track->SetUserInformation(myTrackInfo);
    }
    G4bool myBooleanValue = myTrackInfo->GetHaveBeenTracked();
  

    // Se necessario, setto a true

    if  ((step->GetPreStepPoint()->GetPosition().getZ() < GetZFromMacro()) & (step->GetPostStepPoint()->GetPosition().getZ() >= GetZFromMacro()) & (!myBooleanValue)) {
        // Faccio il tracciamento

        if (Tracker == true) {
            // Valida la procedura. Usare SOLO con pochissime particelle 10-100

            analysis->FillNtupleIColumn(1, 0, G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID());
            analysis->FillNtupleDColumn(1, 1, step->GetPostStepPoint()->GetPosition().getX() / cm);
            analysis->FillNtupleDColumn(1, 2, step->GetPostStepPoint()->GetPosition().getY() / cm);
            analysis->FillNtupleIColumn(1, 3, step->GetTrack()->GetDefinition()->GetPDGEncoding());
            analysis->FillNtupleIColumn(1, 4, step->GetTrack()->GetTrackID());
            analysis->FillNtupleDColumn(1, 5, step->GetTrack()->GetTotalEnergy() / MeV);

            analysis->AddNtupleRow(1);
        }

        if (TrackerHisto == true) {
            /*
            if (step->GetTrack()->GetDefinition()->GetPDGEncoding() == 11) {
                // I am an electron
            }
            if (step->GetTrack()->GetDefinition()->GetPDGEncoding() == 22) {
                // I am a photon
            }
            */

            G4double xpos = step->GetPostStepPoint()->GetPosition().getX() / cm;
            G4double ypos = step->GetPostStepPoint()->GetPosition().getY() / cm;
            G4double weight = step->GetTrack()->GetTotalEnergy() / GeV;

            analysis->FillH2(analysis->GetH2Id("crossingPos"), xpos, ypos);
            analysis->FillH2(analysis->GetH2Id("crossingWeigh"), xpos, ypos, weight);
        }
        // La annoto per non entrare più in questo if per questa sotto particella
        myTrackInfo->SetHaveBeenTracked(true);

        // Aggiorno l'informazione dell'oggetto traccia
        // track->SetUserInformation(myTrackInfo);
    }
  }

  // leo se voglio deposito energetico e basta, senza tracciare

  if ((step->GetPostStepPoint()->GetPosition().getZ() >= GetZFromMacro()) & (DepEn == true) &
      (((pow(step->GetPostStepPoint()->GetPosition().getX(), 2) + pow(step->GetPostStepPoint()->GetPosition().getY(), 2))) >= GetRadiusFromMacro())) {
      // if ((step->GetPostStepPoint()->GetPosition().getZ() >= GetZprofile()) & (! myBooleanValue )){
      // Faccio il tracciamento

      // Valida la procedura. Usare SOLO con pochissime particelle 10-100
      /*
      analysis->FillNtupleIColumn(1, 0, G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID());
      analysis->FillNtupleDColumn(1, 1, step->GetPostStepPoint()->GetPosition().getX()/cm);
      analysis->FillNtupleDColumn(1, 2, step->GetPostStepPoint()->GetPosition().getY()/cm);
      analysis->FillNtupleIColumn(1, 3, step->GetTrack()->GetDefinition()->GetPDGEncoding());
      analysis->FillNtupleIColumn(1, 4, step->GetTrack()->GetTrackID());
      analysis->FillNtupleDColumn(1, 5, step->GetTrack()->GetTotalEnergy()/MeV);




      analysis->AddNtupleRow(1);
      */

      /*
      if (step->GetTrack()->GetDefinition()->GetPDGEncoding() == 11) {
          // I am an electron
      }
      if (step->GetTrack()->GetDefinition()->GetPDGEncoding() == 22) {
          // I am a photon
      }
      */
      // leo G4double xpos = step->GetPostStepPoint()->GetPosition().getX() / cm;
      // leo G4double ypos = step->GetPostStepPoint()->GetPosition().getY() / cm;
      // leo G4double weight = step->GetTrack()->GetTotalEnergy() / GeV;

      // leo analysis->FillH2(analysis->GetH2Id("crossingPos"), xpos, ypos);
      // leo analysis->FillH2(analysis->GetH2Id("crossingWeigh"), xpos, ypos, weight);

      // La annoto per non entrare più in questo if per questa sotto particella
      // leo myTrackInfo->SetHaveBeenTracked(true);

      // Aggiorno l'informazione dell'oggetto traccia
      // track->SetUserInformation(myTrackInfo);

      // leo deposito energetico

      // leo mi conservo le seguenti informazioni:

      analysis->FillNtupleIColumn(2, 0, G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID());

      analysis->FillNtupleIColumn(2, 1, step->GetTrack()->GetDefinition()->GetPDGEncoding());

      analysis->FillNtupleIColumn(2, 2, step->GetTrack()->GetTrackID());

      analysis->FillNtupleDColumn(2, 3, step->GetTrack()->GetTotalEnergy() / MeV);

      // leo uccido la particella una volta che è nella zona di mio interesse

      track->SetTrackStatus(fStopAndKill);

      analysis->AddNtupleRow(2);
  }

// leo quello che segue è un pezzo di sim APC (ste)
    // if (fScoringVolume.size()==0) {
    //   const DetectorConstruction* detConstruction
    //     = static_cast<const DetectorConstruction*>
    //       (G4RunManager::GetRunManager()->GetUserDetectorConstruction());
    //   fScoringVolume = detConstruction->GetScoringVolume();
    // }
    // G4LogicalVolume* volume
    //     = step->GetPreStepPoint()->GetTouchableHandle()
    //       ->GetVolume()->GetLogicalVolume();
    // G4ThreeVector MomentumDirection;
    // G4int VolumeNumber = fScoringVolume.size();
    // // //if volume enter
    // // if (step->GetPostStepPoint()-> GetStepStatus()==G4StepStatus::fGeomBoundary)
    // // {
        // // for (int i = 0; i < VolumeNumber-1; i++)
        // // {
            // // if(volume==fScoringVolume[i])
            // // {
                // // //writing data of each volume from the list fScoringVolume
                // // std::ostringstream ss_z;
                // // ss_z.setf(std::ios::scientific);
                // // ss_z<<volume->GetName()<<" "<<std::flush;
                // // ss_z<<step->GetTrack()->GetTrackID()<<" "<<std::flush;//ticle IDpar
                // // ss_z<<step->GetTrack()->GetDefinition()->GetParticleName()<<" "<<std::flush;//particle name
                // // ss_z<<step->GetPostStepPoint()->GetPosition().getX()/mm<<" "<<std::flush; //coordinate x
                // // ss_z<<step->GetPostStepPoint()->GetPosition().getY()/mm<<" "<<std::flush; //coordinate y
                // // ss_z<<step->GetPostStepPoint()->GetPosition().getZ()/mm<<" "<<std::flush; //coordinate z
                // // MomentumDirection = step->GetPostStepPoint()->GetMomentumDirection();
                // // ss_z<<atan(MomentumDirection.getX()/MomentumDirection.getZ())<<" "<<std::flush; //horizontal angle
                // // ss_z<<atan(MomentumDirection.getY()/MomentumDirection.getZ())<<" "<<std::flush; //vertical angle
                // // ss_z<<step->GetPostStepPoint()->GetKineticEnergy()/MeV<<G4endl; //kinetic energy
                // // fEventAction->AddOutputVolumeBoundary(ss_z.str());
            // // }
        // // }
    // // }
	// Riempimento energie integrate per singolo detector
    // if(volume->GetName()=="S1")
    // {
    //     fEventAction->AddDepositedEnergyS1(step->GetTotalEnergyDeposit()/MeV);
		
		
	// 	// === Provo a contare i fotoni che entrano in S1 ===
		
	// 	// Se entra nel primo scintillatore
	// 	if (step->GetPreStepPoint()-> GetStepStatus()==G4StepStatus::fGeomBoundary)
	// 	{
	// 		// Se è un fotone
	// 		if (step->GetTrack()->GetDefinition()->GetParticleName()=="gamma")
	// 		{
	// 			// Se è stato creato dal primo elettrone
	// 			if (step->GetTrack()->GetParentID() == 1)
	// 			{
	// 				std::ostringstream ss_z;
	// 				ss_z.setf(std::ios::scientific);
					
	// 				ss_z<<volume->GetName()<<" "<<std::flush;										// Volume name
	// 				ss_z<<step->GetTrack()->GetTrackID()<<" "<<std::flush; 							// Particle ID
	// 				ss_z<<step->GetTrack()->GetParentID()<<" "<<std::flush; 						// Parent ID
	// 				ss_z<<step->GetTrack()->GetDefinition()->GetParticleName()<<" "<<std::flush;	//particle name
	// 				ss_z<<G4RunManager::GetRunManager()->GetCurrentEvent()->GetEventID()<<" "<<std::flush;		//event number
	// 				ss_z<<step->GetPreStepPoint()->GetKineticEnergy()/MeV<<G4endl; 					// kinetic energy
	// 				fEventAction->AddOutputVolumeBoundary(ss_z.str());
					
					
	// 				// Lo conto
					
	// 				fEventAction->countGamma();	// Lo conto
					
	// 			}
	// 		}
			
	// 	}

    // }

    // if(volume->GetName()=="S2")
    // {
    //     fEventAction->AddDepositedEnergyS2(step->GetTotalEnergyDeposit()/MeV);
    // }

    // if(volume->GetName()=="Rino")
    // {
    //     fEventAction->AddDepositedEnergy(step->GetTotalEnergyDeposit()/MeV);
    // }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//}