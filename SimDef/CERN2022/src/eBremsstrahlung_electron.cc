﻿// -------------------------------------------------------------------
//
// GEANT4 Class file
//
// This file implements the class for the modified bremsstrahlung process.
// Class based on G4eBremsstrahlung.
//
// -------------------------------------------------------------------
// Some include...

#include "eBremsstrahlung_electron.hh"
#include "G4SystemOfUnits.hh"
#include "G4Gamma.hh"
#include "SeltzerBergerModel_electron.hh"
#include "eBremsstrahlungRelModel_electron.hh"
#include "G4SeltzerBergerModel.hh"
#include "G4eBremsstrahlungRelModel.hh"
#include "G4UnitsTable.hh"
#include "G4LossTableManager.hh"

#include "G4ProductionCutsTable.hh"
#include "G4MaterialCutsCouple.hh"
#include "G4EmParameters.hh"
#include "LogicalCrystalVolume.hh"

// This is a later addition and is important to allow the localized modification
// of the G4 processes (in specified regions)

#include "G4RegionStore.hh"
#include "G4EmConfigurator.hh"

using namespace std;
 
eBremsstrahlung_electron::eBremsstrahlung_electron(const G4String& name):
  G4VEnergyLossProcess(name), 
  isInitialised(false)
{
  SetProcessSubType(fBremsstrahlung);
  SetSecondaryParticle(G4Gamma::Gamma());
  SetIonisation(false);
}

eBremsstrahlung_electron::~eBremsstrahlung_electron()
{}

G4bool eBremsstrahlung_electron::IsApplicable(const G4ParticleDefinition& p)
{
  return (&p == G4Electron::Electron());
}

void 
eBremsstrahlung_electron::InitialiseEnergyLossProcess(const G4ParticleDefinition*,
					       const G4ParticleDefinition*)
{
  if(!isInitialised) {
    // Instantiate the parameters for the bremsstrahlung model
    G4EmParameters* param = G4EmParameters::Instance();
    G4double emin = param->MinKinEnergy();
    G4double emax = param->MaxKinEnergy();
    G4VEmFluctuationModel* fm = nullptr;

    // Activate the model for the world: G4 standard process (low-energy)
    if (!EmModel(0)) { SetEmModel(new G4SeltzerBergerModel()); }
    // if (!EmModel(0)) { SetEmModel(new SeltzerBergerModelElectron()); }
    //                // Uncomment the previous line to go back to the SF process

    EmModel(0)->SetLowEnergyLimit(emin);
    G4double energyLimit = std::min(EmModel(0)->HighEnergyLimit(), GeV);
    EmModel(0)->SetHighEnergyLimit(energyLimit);
    EmModel(0)->SetSecondaryThreshold(param->BremsstrahlungTh());
    EmModel(0)->SetLPMFlag(false);
    AddEmModel(1,EmModel(0),fm);

    // Activate the model for the world: G4 standard process (high-energy)
    if(emax > energyLimit) {
      // Define model
      if (!EmModel(1)) { SetEmModel(new G4eBremsstrahlungRelModel()); }
      // if (!EmModel(1)) { SetEmModel(new eBremsstrahlungRelModel_electron()); }
      //                // Uncomment the previous line to go back to the SF process

      // Set parameters and activate
      EmModel(1)->SetLowEnergyLimit(energyLimit);
      EmModel(1)->SetHighEnergyLimit(emax); 
      EmModel(1)->SetSecondaryThreshold(param->BremsstrahlungTh());
      EmModel(1)->SetLPMFlag(param->LPM());
      AddEmModel(1,EmModel(1),fm);
    }
    
    // Flag for initialization of the process
    isInitialised = true;
  }
}

// Some other functions...

void eBremsstrahlung_electron::StreamProcessInfo(std::ostream& out) const
{
  if(EmModel(0)) {
    G4EmParameters* param = G4EmParameters::Instance();
    G4double eth = param->BremsstrahlungTh(); 
    out << "      LPM flag: " << param->LPM() << " for E > " 
	<< EmModel(0)->HighEnergyLimit()/GeV << " GeV";
    if(eth < DBL_MAX) { 
      out << ",  VertexHighEnergyTh(GeV)= " << eth/GeV; 
    }
    out << G4endl;
  }
}

void eBremsstrahlung_electron::ProcessDescription(std::ostream& out) const
{
  out << " For electrons Bremsstrahlung in crystal ";
  G4VEnergyLossProcess::ProcessDescription(out);
}

// From here onwards: old copies of the code. Do not uncomment!.
//
//
//G4double eBremsstrahlung_electron::PostStepGetPhysicalInteractionLength(
//                             const G4Track& track,
//                             G4double   previousStepSize,
//                             G4ForceCondition* condition)
//{
//    G4double x= DBL_MAX;
//    G4LogicalVolume* aLV = track.GetVolume()->GetLogicalVolume();
//    G4LogicalVolume* aNLV = track.GetNextVolume()->GetLogicalVolume();
//
//    if(LogicalCrystalVolume::IsLattice(aLV) == true &&
//            LogicalCrystalVolume::IsLattice(aNLV) == true){
//        return G4VEnergyLossProcess::PostStepGetPhysicalInteractionLength(track,previousStepSize,condition);
//    }
//    else{
//        x= DBL_MAX;
//    }
//    return x;
//}
//
//G4VParticleChange* eBremsstrahlung_electron::PostStepDoIt(const G4Track& track,
//                                                      const G4Step& step)
//{
//    G4LogicalVolume* aLV = track.GetVolume()->GetLogicalVolume();
//    G4LogicalVolume* aNLV = track.GetNextVolume()->GetLogicalVolume();
//
//    if(LogicalCrystalVolume::IsLattice(aLV) == true &&
//            LogicalCrystalVolume::IsLattice(aNLV) == true){
//        return G4VEnergyLossProcess::PostStepDoIt(track,step);
//    }
//    else
//    {
//        theNumberOfInteractionLengthLeft = -1.0;
//        mfpKinEnergy = currentInteractionLength = DBL_MAX;
//        fParticleChange.InitializeForPostStep(track);
//        return &fParticleChange;
//
//    }
//}
//
//G4double eBremsstrahlung_electron::AlongStepGetPhysicalInteractionLength(
//        const G4Track& track,
//        G4double  previousStepSize,
//        G4double  currentMinimumStep,
//        G4double& currentSafety,
//        G4GPILSelection* selection)
//{
//  G4double x = DBL_MAX;
//  x =G4VEnergyLossProcess::AlongStepGetPhysicalInteractionLength(track,previousStepSize,currentMinimumStep,currentSafety,selection);
//  return x;
//}
//
//G4VParticleChange* eBremsstrahlung_electron::AlongStepDoIt(const G4Track& track,
//                                                       const G4Step& step)
//{
//  fParticleChange.InitializeForAlongStep(track);
//  // The process has range table - calculate energy loss
//
//  return G4VEnergyLossProcess::AlongStepDoIt(track, step);
//  /*
//  if(-1 < verboseLevel) {
//    G4double del = finalT + eloss + esec - preStepKinEnergy;
//    G4cout << "Final value eloss(MeV)= " << eloss/MeV
//           << " preStepKinEnergy= " << preStepKinEnergy
//           << " postStepKinEnergy= " << finalT
//           << " de(keV)= " << del/keV
//           << " lossFlag= " << lossFluctuationFlag
//           << "  status= " << track.GetTrackStatus()
//           << G4endl;
//  }
//  */
//  return &fParticleChange;
//}
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo....