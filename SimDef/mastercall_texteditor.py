# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# The aim of this script is to modify the g4oreo simulation source code, in order to implement:
# operation mode (axial/random), statistics to be acquired, number of runs to call, etc.
# Note: NEVER CALL THIS SCRIPT ALONE! It is called inside the mastercall.sh script, so check
# that out instead of this.
#
# ==========================================================================================
#                             IMPORT OF THE EXTERNAL PYTHON MODULES
# ==========================================================================================
import os
import sys
import numpy as np

# ==========================================================================================
#                                         INPUTS
# ==========================================================================================
# Define the parameters for the execution of the script
mode = sys.argv[1]               # Operation mode: either "axial" or "amorphous"
total_stat = int(sys.argv[2])    # How many events to acquire in total
partial_stat = int(sys.argv[3])  # How many events to acquire in each output file
printoutflag = sys.argv[4]       # Flag for printout of the current number of events

# Consistency check
if partial_stat > total_stat:
    # If the partial statistics is larger than the total statistics to be acquired,
    # then trust the partial stat.
    total_stat = partial_stat

# ==========================================================================================
#                                   PARAMETERS AND PATHS
# ==========================================================================================
# Define where the "main.cc" file is located and at which lines the code must be modified in
# order to implement the required mode of operation.
thisfolder = os.path.dirname(os.path.abspath(__file__))
main_file = thisfolder + "/CERN2022/main.cc"
line_bChanneling = 61-1

# Define where the "RunAction.cc" file is located and at which lines the code must be modified
# in order to implement the printoutflag.
RunAction_file = thisfolder + "/CERN2022/src/RunAction.cc"
line_printoutflag = 28-1

# Define where the macros to run the simulation are located and at which lines the code must be
# modified, in order to acquire the desired statistics.
macro_file = thisfolder + "/CERN2022/macros/run_forphysics.mac"
line_run = 27-1

# ==========================================================================================
#                               EDIT THE SIMULATION CODE
# ==========================================================================================
# Open the "main.cc" and modify the mode of operation
with open(main_file,'r') as dc:
    # Read all the code lines
    print(f"Editing main.cc file")
    lines = dc.readlines()

    # Modify the required line in order to define whether the crystal must be considered
    # as oriented or not
    if (mode == 'axial'):
        lines[line_bChanneling] = '\tG4bool bChanneling = true;\n'
    else:
        lines[line_bChanneling] = '\tG4bool bChanneling = false;\n'

with open(main_file,'w') as dc:
    # Write the lines
    dc.writelines(lines)

# ==========================================================================================

# Open the RunAction.cc file and modify the progress printout flag
with open(RunAction_file,'r') as dc:
    # Read all the code lines
    print(f"Editing RunAction.cc file")
    lines = dc.readlines()

    # Modify the required line in order to define the number of events at which a flag must
    # be printed for user warning (e.g., every 100 events)
    lines[line_printoutflag] = '\tG4int NumofPrintProgress = ' + printoutflag + ';\n'

with open(RunAction_file,'w') as dc:
    # Write the lines
    dc.writelines(lines)

# ==========================================================================================
# Open the simulation macro and modify the number of runs to perform (and the number of 
# events per single run)
numofruns = int(np.floor(total_stat/partial_stat))
partial_stat_lastrun = int(total_stat - partial_stat*numofruns)

with open(macro_file,'r') as dc:
    # Read all the code lines
    print(f"Editing macro file")
    lines = dc.readlines()

    # Add as many lines as it is required to perform the simulation.
    writestring = '/run/beamOn ' + str(partial_stat) + '\n'
    lines = lines[:line_run]         # Crop any exceeding line
    for _ in range(numofruns):
        lines.append(writestring)    # Add new beamOn commands

    # If total_stat/partial_stat does not give an integer number as result,
    # it is necessary to add a last run for completing the work.
    if partial_stat_lastrun != 0:
        writestring_lastrun = '/run/beamOn ' + str(partial_stat_lastrun) + '\n'
        lines.append(writestring_lastrun)

with open(macro_file,'w') as dc:
    # Write the lines
    dc.writelines(lines)

# Close the script
print(f"Python task completed! \n")
